echo off
cd %CD%
set path=%path%;%SystemRoot%\Microsoft.NET\Framework\v3.5
echo clearing previous output
IF EXIST unified-map-b (
rmdir unified-map-b /s/q
)
IF EXIST topic-set-a (
rmdir topic-set-a /s/q
)
IF EXIST unified-map-B-otcs (
rmdir unified-map-B-otcs /s/q
)
echo starting 1 of 3
..\..\bin\deltaxml-dita.exe compare map doc1/maps/main.ditamap doc2/maps/main.ditamap topic-set-A map-result-origin=A map-result-structure=topic-set
echo starting 2 of 3
..\..\bin\deltaxml-dita.exe compare map doc1/maps/main.ditamap doc2/maps/main.ditamap unified-map-B map-result-origin=B map-result-structure=unified-map
echo starting 3 of 3
..\..\bin\deltaxml-dita.exe compare map doc1/maps/main.ditamap doc2/maps/main.ditamap unified-map-B-otcs map-result-origin=B map-result-structure=unified-map output-format=oxygen-tcs
echo complete
pause
