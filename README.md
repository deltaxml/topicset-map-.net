# Topicset Map sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DITA-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the exe files.*

*For example `DeltaXML-DITA-Compare-8_0_0_n/samples/sample-name`.*

---

## Summary

This sample compares two DITA maps, doc1/maps/main.ditamap and doc2/maps/main.ditamap, using all three of the available map result structures: the 'topic set', the 'map-pair' and the 'unified-map'. This sample compares two DITA maps, doc1/maps/main.ditamap and doc2/maps/main.ditamap, using all three of the available map result structures: the 'topic set', the 'map-pair' and the 'unified-map'. For more details see: [Topicset Map documentation](https://docs.deltaxml.com/dita-compare/latest/topicset-map-sample-3801205.html).

## Using a Batch File

Run the rundemo.bat batch file either by entering rundemo from the command-line or by double-clicking on this file from Windows Explorer. This script runs the same comparison 3 times with different output settings.

## Running the sample from the Command line

The sample can be run directly by issuing the following command

    ..\..\bin\deltaxml-dita.exe compare map doc1\maps\main.ditamap doc2\maps\main.ditamap topic-set-A map-result-origin=A map-result-structure=topic-set

The 'map pair' or 'unified map' comparisons can be run in a similar manner to the 'topic set', but in this case the map-result-orign is B, the map-result-structure is map-pair or unified-map, and the map-copy-location is map-pair-B or unified-map-B. Further, it is possible to select the oXygen tracked changes output format, by adding the parameter output-format=oxygen-tcs to the arguments.